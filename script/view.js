var idsDrawing = ['red', 'green', 'white', 'blue', 'purple', 'yellow'];

function startGame(json) {
    var targetGame = document.getElementById('game');
    var game = createGame(targetGame,
        json.stages,
        document.getElementById('modal'),
        document.getElementById('congrats'),
        modifyLight);
}

function changeBackgroundOut(event) {
    var target = event.target;
    if (!!target) {
        target.style['background-position-y'] = '50%';
    }
}

function changeBackgroundY(event) {
    var target = event.target;
    if (!!target) {
        var rect = target.getBoundingClientRect();
        var offset = {
            top: rect.top + document.body.scrollTop,
            // left: rect.left + document.body.scrollLeft,

        };
        // var relativeX = (event.pageX - offset.left);
        var relativeY = (event.pageY - offset.top);
        relativeY = relativeY / rect.height * 100;
        target.style['background-position-y'] = relativeY + '%';
    }

}

function modifyLight(colorsP) {
    // var strColors = '';
    var idsCheck = idsDrawing.slice();
    for (const color in colorsP) {
        idsCheck.splice(color, 1);
        removeClass(document.getElementById(color), 'hidden');
        if (colorsP.hasOwnProperty(color)) {
            const value = colorsP[color];
            // strColors += color + ' ' + value + '<br>';
            if (value) {
                addClass(document.getElementById(color), 'light--on');
            } else {
                removeClass(document.getElementById(color), 'light--on');
            }
        }
    }
    for (let index = 0; index < idsCheck.length; index++) {
        const element = idsCheck[index];
        addClass(document.getElementById(element), 'hidden');
    }
}

function afterLevelLoad(json) {
    removeClass(document.getElementById('instructionsReady'), 'hidden');

    document.getElementById("instructions").
        addEventListener('click', function () {
            startGame(json);
            addClass(document.getElementById('instructions'), 'hidden');
            makeFullScreen();
        });

    document.getElementById("playAgain").
        addEventListener('click', function () {
            startGame(json);
            addClass(document.getElementById('congrats'), 'hidden');
            makeFullScreen();
        });

    var lights = [...document.querySelectorAll('.light')];
    addEventListenerList(lights, 'mousemove', changeBackgroundY);
    addEventListenerList(lights, 'mouseout', changeBackgroundOut);
}