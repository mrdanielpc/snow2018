function createGame(targetGame, stages, modal, congrats, callback) {
    var _movements = {};
    var indexStage = 0;
    var _previousPoint;
    var _usableCells;
    var _color;
    var _cells;
    var stage = new Stage(stages[indexStage]);
    var _callback = callback;
    var callbackColors;
    var targetBoard = document.getElementById('board');
    removeClass(targetGame, 'hidden');
    createBoard(targetBoard, stage);
    window.scrollTo(0, 100);
    function mouseDownCells(ev) {
        ev.preventDefault();
        _previousPoint = calculatePoint(_cells, ev.currentTarget, stage.cellsHorizontal);

        var classes = ev.currentTarget.className;
        _color = classes.substring(classes.indexOf('cell--color-') + 'cell--color-'.length,
            classes.indexOf(' ', classes.indexOf('cell--color-')));

        // _movements = [];
        if (!!_movements[_color] && _movements[_color].length > 1) {
            if (ev.target === _movements[_color][0].target ||
                ev.target === _movements[_color][_movements[_color].length - 1].target) {
                // si es un final
                removeMovement(_color);
            } else {
                removeMovementUntilTarget(ev.target, _color);
            }
            callbackColors[_color] = false;
            _callback(callbackColors);
        } else {
            _movements[_color] = [];
        }
        _movements[_color].push(_previousPoint);

        _usableCells = [...document.querySelectorAll('.cell.js-cell--empty'),
        ...document.querySelectorAll('.cell--color-' + _color),

        ];

        addEventListenerList(_usableCells, 'mousemove', mouseMoveWithColor);
        addEventListenerList(_usableCells, 'touchmove', mouseMoveWithColor);
    }

    function finishMovement(ev) {

        if (!!_usableCells) {
            removeEventListenerList(_usableCells, 'mousemove', mouseMoveWithColor);
            removeEventListenerList(_usableCells, 'touchmove', mouseMoveWithColor);
            _usableCells = undefined;
            if (_movements[_color].length > 1) {
                if (!hasClass(_movements[_color][_movements[_color].length - 1].target, 'cell--end')) {
                    removeMovement(_color);

                } else {
                    //Conectados
                    _movements[_color].forEach((mov) => {
                        addClass(mov.target, 'cell--connected');

                    });
                    callbackColors[_color] = true;
                    _callback(callbackColors);

                    if (checkWin()) {
                        removeListeners();

                        indexStage++;
                        if (indexStage < stages.length) {
                            pinfo('Comenzando<br>el nivel ' + (indexStage + 1) + '/' +
                                stages.length + ' en...', 2900);

                            setTimeout(nextStage, 2800, this);
                        } else {
                            setTimeout(endWin, 1000, this);
                        }

                    } else {
                        addEventListenerList([...document.querySelectorAll('.cell--color-' + _color + ':not(.cell--end)')], 'mousedown', mouseDownCells);
                        addEventListenerList([...document.querySelectorAll('.cell--color-' + _color + ':not(.cell--end)')], 'touchstart', mouseDownCells);
                    }
                }
            }
        }
    }

    function removeListeners() {
        document.removeEventListener('mouseup', finishMovement);
        document.removeEventListener('touchend', finishMovement);
        if (!!_usableCells) {
            removeEventListenerList(_usableCells, 'mousemove', mouseMoveWithColor);
            removeEventListenerList(_usableCells, 'touchmove', mouseMoveWithColor);
        }
        var finalPoints = _cells.filter((cell) => {
            return hasClass(cell, 'cell--end');
        });
        removeEventListenerList(finalPoints, 'mousedown', mouseDownCells);
        removeEventListenerList(finalPoints, 'touchstart', mouseDownCells);
    }

    function mouseMoveWithColor(evData) {
        var realTarget = evData.currentTarget;
        if (!!evData.changedTouches) {
            var myLocation = evData.changedTouches[0];
            realTarget = document.elementFromPoint(myLocation.clientX, myLocation.clientY);
            if (!realTarget) {
                return;
            }
        }
        //Por culpa de los eventos touch tengo que comprobar siempre que no se vaya de mano en el color
        if (!(realTarget.className.indexOf('js-cell--empty') >= 0 ||
            // ((realTarget.className.indexOf('cell--end') >= 0 && realTarget.className.indexOf('cell--color-' + _color) > 0)) ||
            realTarget.className.indexOf('cell--color-' + _color) > 0
        )) {
            return;
        }
        var _newPoint = calculatePoint(_cells, realTarget, stage.cellsHorizontal);
        if (_movements[_color].length >= 2 && _newPoint.index === _movements[_color][_movements[_color].length - 2].index) {
            removeColorDirection(realTarget, _cells[_previousPoint.index], _color);
            _movements[_color].pop();
            _previousPoint = _movements[_color][_movements[_color].length - 1];
        }
        var _dir = adjacentPoint(_newPoint, _previousPoint);
        if (!!_dir) {
            if (hasClass(realTarget, 'js-cell--empty')) {
                addColorDirection(realTarget, _cells[_previousPoint.index], _color, _dir);
                _previousPoint = calculatePoint(_cells, realTarget, stage.cellsHorizontal);
                _movements[_color].push(_previousPoint);
            }
            if (realTarget !== _movements[_color][0].target && hasClass(realTarget, 'cell--end')) {
                if (realTarget !== _movements[_color][0].target)
                    addColorDirection(realTarget, _cells[_previousPoint.index], _color, _dir);
                _previousPoint = calculatePoint(_cells, realTarget, stage.cellsHorizontal);
                _movements[_color].push(_previousPoint);
                finishMovement();
            }
        }
    }
    function removeMovementUntilTarget(target, color) {
        var _breaked = false;
        for (let index = _movements[color].length - 1; index >= 0; index--) {
            const element = _movements[color][index];
            if (element.target === target) {
                _emptyCellDirectionTo(element.target);
                removeClass(element.target, 'cell--connected');
                removeEventListenerList(element.target, 'mousedown', mouseDownCells);
                removeEventListenerList(element.target, 'touchstart', mouseDownCells);
                _breaked = true;
                _movements[color].pop();
            } else {
                if (!_breaked) {
                    _movements[color].pop();
                    if (!hasClass(element.target, 'cell--end')) {
                        emptyCellDirection(element.target);
                        removeClass(element.target, 'cell--color-' + color);
                        addClass(element.target, 'js-cell--empty');
                    } else {
                        emptyCellDirection(element.target);
                    }
                }
                removeClass(element.target, 'cell--connected');
                if (!hasClass(element.target, 'cell--end')) {
                    removeEventListenerList(element.target, 'mousedown', mouseDownCells);
                    removeEventListenerList(element.target, 'touchstart', mouseDownCells);
                }
            }

        }
        // _movements[color] = [];
    }
    function removeMovement(color) {
        _movements[color].forEach((mov) => {
            if (!hasClass(mov.target, 'cell--end')) {
                emptyCellDirection(mov.target);
                removeClass(mov.target, 'cell--color-' + color);
                addClass(mov.target, 'js-cell--empty');
                removeEventListenerList(mov.target, 'mousedown', mouseDownCells);
                removeEventListenerList(mov.target, 'touchstart', mouseDownCells);
            } else {
                emptyCellDirection(mov.target);
            }
            removeClass(mov.target, 'cell--connected');

        });
        _movements[color] = [];

    }

    function addColorDirection(currentTarget, previous, color, direction) {
        removeClass(currentTarget, 'js-cell--empty');
        addClass(currentTarget, 'cell--color-' + color);
        addClass(currentTarget, direction.P2);
        addClass(previous, direction.P1);
    }

    function removeColorDirection(currentTarget, previous, color) {
        removeClass(previous, directions.fromBottom);
        removeClass(previous, directions.fromTop);
        removeClass(previous, directions.fromLeft);
        removeClass(previous, directions.fromRight);

        removeClass(currentTarget, directions.toBottom);
        removeClass(currentTarget, directions.toTop);
        removeClass(currentTarget, directions.toLeft);
        removeClass(currentTarget, directions.toRight);

        removeClass(previous, 'cell--color-' + color);


        addClass(previous, 'js-cell--empty');

    }
    function _emptyCellDirectionTo(target) {
        removeClass(target, directions.toBottom);
        removeClass(target, directions.toTop);
        removeClass(target, directions.toLeft);
        removeClass(target, directions.toRight);
    }
    function emptyCellDirection(target) {
        _emptyCellDirectionTo(target);
        removeClass(target, directions.fromBottom);
        removeClass(target, directions.fromTop);
        removeClass(target, directions.fromLeft);
        removeClass(target, directions.fromRight);
    }

    function checkWin() {
        var win = true;
        stage.colors.forEach((col) => {
            if (_movements[col].length <= 1) {
                win = false;
            }
        });
        return win;
    }

    function createBoard(targetBoard, stage) {
        var transposeMirror = Math.floor(Math.random() * (3 + 1));
        // var supportCssGrid = typeof targetBoard.style.grid === 'string' || typeof targetBoard.style.msGridRows === 'string';
        if (transposeMirror & mirrorEnum.transpose) {
            var tmp = stage.cellsHorizontal;
            stage.cellsHorizontal = stage.cellsVertical;
            stage.cellsVertical = tmp;
        }
        var cellsCount = stage.cellsHorizontal * stage.cellsVertical;
        var templateHorizontal = '';
        var templateVertical = '';
        for (let index = 0; index < stage.cellsHorizontal; index++) {
            templateHorizontal = '1fr ' + templateHorizontal;
        }
        for (let index = 0; index < stage.cellsVertical; index++) {
            templateVertical = '1fr ' + templateVertical;
        }

        targetBoard.style.gridTemplateColumns = templateHorizontal;
        targetBoard.style.msGridColumns = templateHorizontal;
        targetBoard.style.msGridRows = templateVertical;


        while (targetBoard.lastChild) {
            targetBoard.removeChild(targetBoard.lastChild);
        }
        var cellsColors = [];
        var maxIndex = stage.cellsVertical * stage.cellsHorizontal - 1;

        stage.points.forEach((point, ind) => {
            var index1;
            var index2;
            if (transposeMirror & mirrorEnum.transpose) {
                index1 = point.y + (point.x) * stage.cellsHorizontal;
                index2 = point.yFin + (point.xFin) * stage.cellsHorizontal;
            } else {
                index1 = point.x + (point.y) * stage.cellsHorizontal;
                index2 = point.xFin + (point.yFin) * stage.cellsHorizontal;
            }
            if (transposeMirror & mirrorEnum.mirror) {
                index1 = maxIndex - index1;
                index2 = maxIndex - index2;
            }
            cellsColors[index1] = stage.colors[ind];
            cellsColors[index2] = stage.colors[ind];
        });
        for (let index = 0; index < cellsCount; index++) {
            var div = document.createElement('div');
            addClass(div, 'cell');
            var index2 = index + 1;

            var x = index2 % stage.cellsHorizontal === 0 ? stage.cellsHorizontal : index2 % stage.cellsHorizontal;
            var y = Math.ceil(index2 / stage.cellsHorizontal);

            div.style.msGridColumn = x;
            div.style.msGridRow = y;

            if (!!cellsColors[index]) {
                addClass(div, 'cell--color-' + cellsColors[index]);
                addClass(div, 'cell--end');
            } else {
                addClass(div, 'js-cell--empty');
            }
            // if (!supportCssGrid) {
            div.style.width = (100 / stage.cellsHorizontal) + '%';
            div.style.height = (100 / stage.cellsVertical) + '%';
            // }
            targetBoard.appendChild(div);
        }

        initVariablesAndEvents();
    }

    function initVariablesAndEvents() {
        // pinfo('Nivel ' + (indexStage + 1));
        _cells = [...document.querySelectorAll('.cell')];
        // stage.colors.forEach((col) => {
        //             if (!callbackColors[col]) {
        //                 callbackColors[col] = {
        //                     connected: 0,
        //                     maxConnected: 1
        //                 }
        //             } else {
        //                 callbackColors[col].maxConnected++;
        //             }
        //         });
        //     });
        //     _callback(callbackColors);
        callbackColors = [];
        stage.colors.forEach((col) => {
            callbackColors[col] = false;
            _movements[col] = [];

        });
        _callback(callbackColors);

        var finalPoints = _cells.filter((cell) => {
            return hasClass(cell, 'cell--end');
        });
        addEventListenerList(finalPoints, 'mousedown', mouseDownCells);
        addEventListenerList(finalPoints, 'touchstart', mouseDownCells);

        document.addEventListener('mouseup', finishMovement);
        document.addEventListener('touchend', finishMovement);
    }

    function nextStage() {
        // indexStage++;
        // if (indexStage < stages.length) {
        stage = new Stage(stages[indexStage]);
        createBoard(targetBoard, stage);
        // } else {
        //     pinfo('you win!');
        // }
    }

    function endWin() {
        removeClass(congrats, 'hidden');
        addClass(targetGame, 'hidden');
    }

    function pinfo(str, time) {
        var pinfoD = document.getElementById('info');
        removeClass(modal, 'hidden');
        if (!!pinfoD) {
            pinfoD.innerHTML = str;
        }
        setTimeout(() => {
            addClass(modal, 'hidden');
        }, time);
    }
    return this;

}