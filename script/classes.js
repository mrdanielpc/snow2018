class Stage {
    constructor({ name, points, cellsHorizontal, cellsVertical, colors }) {
        this.name = name;
        this.points = points;
        this.cellsHorizontal = cellsHorizontal;
        this.cellsVertical = cellsVertical;
        this.colors = colors;
    }
    toString() {
        return '(' + this.name + ')';
    }
}

var mirrorEnum = {
    mirror: 1,
    transpose: 2,
};