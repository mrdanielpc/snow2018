var
	autoprefixer = require('gulp-autoprefixer'),
	babel = require('gulp-babel'),
	chmod = require('gulp-chmod'),
	convertEncoding = require('gulp-convert-encoding'),
	concat = require('gulp-concat'),
	del = require('del'),
	gulp = require('gulp'),
	notify = require('gulp-notify'),
	plumber = require('gulp-plumber'),
	sass = require('gulp-sass'),
	sourcemaps = require('gulp-sourcemaps'),
	uglify = require('gulp-uglify');

const BABEL_POLYFILL = './node_modules/babel-polyfill/browser.js';

var libs = [
	BABEL_POLYFILL,
	'script/libs/*.js'
],
	jsPrimary = [
		'script/*.js'
	];

//FUNCION  DE ERROR DE HUGO, CON EL GULP-NOTIFY
var onError = function (err) {
	notify.onError({
		title: "Gulp",
		subtitle: "Failure!",
		message: "Error: <%= error.message %>",
		sound: "Beep"
	})(err);

	this.emit('end');
};

gulp.task('js', function () {

	console.log('Eliminando todos los ficheros de la carpeta ./dist');
	del.sync('./dist/*.js'); //S�lo eliminamos los js al estar en la tarea de js ( obvio )
	console.log('Ficheros eliminados');

	gulp.src(libs)
		.pipe(plumber({
			errorHandler: onError
		}))
		.pipe(concat('libs.js'))

		.pipe(gulp.dest('./dist'));

	console.log(jsPrimary);

	gulp.src(jsPrimary)
		.pipe(sourcemaps.init())
		.pipe(plumber({
			errorHandler: onError
		}))
		.pipe(babel({
			presets: ['@babel/env']
		}))
		.pipe(concat('game.js'))
		.pipe(sourcemaps.write('.'))


		.pipe(gulp.dest('./dist'));

});

gulp.task('jspro', function () {

	console.log('Eliminando todos los ficheros de la carpeta ./dist');
	del.sync('./dist/*.js'); //S�lo eliminamos los js al estar en la tarea de js ( obvio )
	console.log('Ficheros eliminados');

	gulp.src(libs)
		.pipe(plumber({
			errorHandler: onError
		}))
		.pipe(concat('libs.js'))

		.pipe(gulp.dest('./dist'));

	console.log(jsPrimary);

	gulp.src(jsPrimary)
		.pipe(plumber({
			errorHandler: onError
		}))
		.pipe(babel({
			presets: ['@babel/env']
		}))
		.pipe(concat('game.js'))
		.pipe(uglify())
		.pipe(gulp.dest('./dist'));

});
gulp.task('css', function () {

	del.sync('./dist/*.css'); //S�lo eliminamos los css al estar en la tarea de css ( obvio )

	gulp.src(['./scss/main.scss'])
		//  gulp.src(['./scss/**/main.scss'])
		.pipe(plumber({
			errorHandler: onError
		}))
		.pipe(sass({
			errLogToConsole: true
		}))
		.pipe(concat('main.css'))
		.pipe(autoprefixer({
			grid: true,
			browsers: ['last 40 versions'],
			cascade: false
		}))
		.pipe(gulp.dest('./dist'));

});

gulp.task('printcss', function () {

	del.sync('./dist/*.css'); //S�lo eliminamos los js al estar en la tarea de js ( obvio )

	gulp.src('./scss/**/print.scss')
		.pipe(plumber({
			errorHandler: onError
		}))
		.pipe(sass({
			errLogToConsole: true
		}))
		.pipe(concat('print.css'))
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}))
		.pipe(gulp.dest('./dist'));

});
gulp.task('images', function () {

	del.sync('./dist/img/*.*'); //S�lo eliminamos los js al estar en la tarea de js ( obvio )

	gulp.src('./img/*.*')
		.pipe(plumber({
			errorHandler: onError
		}))
		.pipe(gulp.dest('./dist/img'));

});
gulp.task('levels', function () {

	del.sync('./dist/levels.json'); //S�lo eliminamos los js al estar en la tarea de js ( obvio )

	gulp.src('./levels.json')
		.pipe(plumber({
			errorHandler: onError
		}))
		.pipe(gulp.dest('./dist/'));

});
gulp.task('fonts', function () {

	del.sync('./dist/fonts/*.*'); //S�lo eliminamos los js al estar en la tarea de js ( obvio )

	gulp.src('./fonts/*.*')
		.pipe(plumber({
			errorHandler: onError
		}))
		.pipe(gulp.dest('./dist/fonts'));

});

gulp.task('dev', function () {
	del.sync('./dist/*.*');
	gulp.run('css');
	gulp.run('printcss');
	gulp.run('js');
	gulp.run('images');
	gulp.run('levels');
	gulp.run('fonts');

});

gulp.task('pro', function () {
	del.sync('./dist/*.*');
	gulp.run('css');
	gulp.run('printcss');
	gulp.run('jspro');
	gulp.run('images');
	gulp.run('levels');
	gulp.run('fonts');

});

gulp.task('uglify', function () {
	del.sync('./dist/ugly.js');
	gulp.src(['./dist/game.js'])
		.pipe(concat('./dist/ugly.js'))
		.pipe(gulp.dest(''))
		.pipe(uglify())
		.pipe(gulp.dest(''));

});

gulp.task('watch', function () {

	gulp.run('dev');

	gulp.watch(['./script/*.js'], function () {
		gulp.run('js');
	});

	gulp.watch('./scss/**/*.scss', function () {
		gulp.run('css');
		gulp.run('printcss');
	});
	gulp.watch(['./svg/**/*.svg'], function () {
		gulp.run('svg');
	});

});

gulp.task('default', ['watch']);